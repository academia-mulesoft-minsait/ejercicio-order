# Ejercicio For Each Order

Refer to the exhibits. What payload is logged at the end of the main flow?

![alt text](image.png)

```
<?xml version="1.0" encoding="UTF-8"?>

<mule xmlns:http="http://www.mulesoft.org/schema/mule/http" xmlns="http://www.mulesoft.org/schema/mule/core"
	xmlns:doc="http://www.mulesoft.org/schema/mule/documentation"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd
http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd">
	<http:listener-config name="HTTP_Listener_config" doc:name="HTTP Listener config" doc:id="9a5318c7-0fa3-4744-8a36-4c8d871465b9" >
		<http:listener-connection host="0.0.0.0" port="8081" />
	</http:listener-config>
	<flow name="ejercicio-orderFlow" doc:id="49d8973b-7eb4-4f52-a082-edc7806081bb" >
		<http:listener doc:name="HTTP: GET /" doc:id="5487cca1-62b0-4cb1-9fb4-a1e49ac072e3" config-ref="HTTP_Listener_config" path="/practica"/>
		<set-payload value="#[[1,2,3,4]]" doc:name="[1,2,3,4]" doc:id="d7d4603c-202c-4984-b8e4-ce9923b6d977" />
		<foreach doc:name="For Each" doc:id="3fc06f76-2a9a-4fca-b98a-626edfe90448" >
			<set-payload value='#["order" ++ payload]' doc:name="Set Payload" doc:id="ff23e4b9-3ae9-4ad2-8cb4-32e97193cd32" />
		</foreach>
		<logger level="INFO" doc:name="payload" doc:id="5784435b-b82e-45ae-b597-0224bd7d4266" message="#[payload]"/>
	</flow>
</mule>

```

- [ ] order1order2order3order4
- [x] [1,2,3,4]
- [ ] order4
- [ ] [order1, order2, order3, order4]

**Respuesta correcta:** [1,2,3,4]